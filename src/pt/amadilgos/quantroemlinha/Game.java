package pt.amadilgos.quantroemlinha;

/**
 * @author Fernando Sousa e Ruben Marinheiro
 * @version 1.0
 * @since 2018-12-10
 */

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.io.*;

public class Game {

    private static String player1_name = "";
    private static int player1_id = 0;
    private static String player1 = " ";
    private static String player2_name = "";
    private static int player2_id = 0;
    private static String player2 = " ";
    private static String gameRoundInput;
    private static int colPlayer1;
    private static int colPlayer2;
    private static String[][] play = new String[6][7];
    static Scanner userInput = new Scanner(System.in);


    /**
     * Método criado e usado para começar o jogo entre 2 id's (jogadores) diferentes
     * @param registeredPlayer1 id do jogador 1
     * @param registeredPlayer2 id do jogador 2
     */
    public static void gameStartConfig(Player registeredPlayer1, Player registeredPlayer2) {


        do {
            player1_name = registeredPlayer1.playerName;
            player2_name = registeredPlayer2.playerName;
            player1_id = registeredPlayer1.playerId;
            player2_id = registeredPlayer2.playerId;
            System.out.print( player1_name + " , com que símbolo queres jogar, X ou O ? ");
            player1 = userInput.next();
            if (player1.equalsIgnoreCase("x")) {
                player1 = "X";
                player2 = "O";
                System.out.println(player1_name + " : X    |    " + player2_name + " : O");
            } else if (player1.equalsIgnoreCase("o")) {
                player1 = "O";
                player2 = "X";
                System.out.println(player1_name + " : O    |    " + player2_name + " : X");
            }
        } while (!(player1.equalsIgnoreCase("x") || player1.equalsIgnoreCase("o")));

    }

    /**
     * Método para resetar a matriz
     */
    public static void gameTableStart() {

        for (int i = 0; i < play.length; i++) {
            for (int j = 0; j <= play.length; j++) {
                play[i][j] = " ";
            }
        }

        gameTablePrint();
    }

    /**
     * Método para contar numero de rondas
     * @return número da ronda
     */
    public static int gameRoundCount() {
        int count = 0;
        for (int i = 0; i < play.length; i++) {
            for (int j = 0; j <= play.length; j++) {
                if (!play[i][j].equals(" ")) {
                    count ++;
                }
            }
        }
        return count;
    }

    /**
     * Método para sair do jogo
     * @param selectedPlayer nome do jogador que quis sair
     */
    public static void gamePause(String selectedPlayer) {

        try {

            if (!new File("data").exists()) {
                new File("data").mkdir();
            } else {
                String symbol;

                if (selectedPlayer.equalsIgnoreCase("player1")) {
                    symbol = player1;
                } else {
                    symbol = player2;
                }

                BufferedWriter out = new BufferedWriter(new FileWriter("data/GameSave.txt", true));
                BufferedReader in = new BufferedReader(new FileReader("data/GameSave.txt"));
                if (in.ready()) {
                    String[] linha;
                    int convertedNum = 0;

                    while (in.ready()) {
                        linha = in.readLine().split(":");
                        String num = linha[1];
                        convertedNum = Integer.parseInt(num) + 1;
                    }
                    in.close();
                    String output = "GameID:" + convertedNum + ":Player1ID:" + player1_id + ":Player2ID:" + player2_id;
                    for (int i = 0; i < play.length; i++) {
                        for (int j = 0; j <= play.length; j++) {
                            output += ":" + play[i][j];
                        }
                    }

                    output += ":Round:" + (gameRoundCount() + 1) + ":Player:" + selectedPlayer + ":Symbol:" + symbol;

                    out.newLine();
                    out.write(output);
                    out.close();
                    System.out.println("O ID do seu jogo é: " + convertedNum + ", MEMORIZE!");
                    try {
                        Thread.sleep(3000);
                    } catch (Exception ex) {

                    }
                } else {
                    String output = "GameID:1:Player1ID:" + player1_id + ":Player2ID:" + player2_id;
                    for (int i = 0; i < play.length; i++) {
                        for (int j = 0; j <= play.length; j++) {
                            output += ":" + play[i][j];
                        }
                    }

                    output += ":Round:" + (gameRoundCount() + 1) + ":Player:" + selectedPlayer + ":Symbol:" + symbol;

                    out.write(output);
                    out.close();
                    System.out.println("O ID do seu jogo é: 1, MEMORIZE!");
                    try {
                        Thread.sleep(3000);
                    } catch (Exception ex) {

                    }
                }
                Menu.exitMessage();
            }

        } catch (Exception ex) {

        }

    }

    /**
     * Metodo para iniciar de novo o jogo
     * @param gameLoadID id do jogo gravado
     */
    public static void gameSaveLoad(int gameLoadID) {
        try {

            if (!new File("data/GameSave.txt").exists()) {
                System.out.println("Não existe qualquer jogo pendente...");
                Menu.backOrExit();
            } else {

                BufferedReader in = new BufferedReader(new FileReader("data/GameSave.txt"));
                String[] linha;
                String thisLine;
                int counter = 6;

                while (in.ready()) {
                    thisLine = in.readLine();
                    linha = thisLine.split(":");
                    if (linha[1].equals(String.valueOf(gameLoadID))) {
                        for (int i = 0; i < play.length; i++) {
                            for (int j = 0; j <= play.length; j++) {
                                play[i][j] = linha[counter];
                                counter += 1;
                            }
                        }
                    }
                }
                in.close();
            }

        } catch (Exception ex) {

        }
    }

    public static void gameLoadConfig(int gameLoadID) {

        try {
            if (!new File("data/GameSave.txt").exists()) {
                System.out.println("Não existe qualquer jogo pendente...");
                Menu.backOrExit();
            } else {
                BufferedReader in = new BufferedReader(new FileReader("data/GameSave.txt"));
                BufferedWriter tempOut = new BufferedWriter(new FileWriter("data/savetemp.txt", true));
                String[] linha;
                String thisLine;
                int loadPlayer1ID = 0;
                int loadPlayer2ID = 0;
                int roundCount = 0;

                while (in.ready()) {
                    thisLine = in.readLine();
                    linha = thisLine.split(":");
                    if (linha[1].equals(String.valueOf(gameLoadID))) {
                        loadPlayer1ID = Integer.parseInt(linha[3]);
                        loadPlayer2ID = Integer.parseInt(linha[5]);
                        roundCount = Integer.parseInt(linha[49]);
                        if (linha[51].equalsIgnoreCase("player1") && linha[53].equalsIgnoreCase("x")) {
                            player1 = "X";
                            player2 = "O";
                        } else if (linha[51].equalsIgnoreCase("player1") && linha[53].equalsIgnoreCase("o")) {
                            player1 = "O";
                            player2 = "X";
                        } else if (linha[51].equalsIgnoreCase("player2") && linha[53].equalsIgnoreCase("x")) {
                            player2 = "X";
                            player1 = "O";
                        } else if (linha[51].equalsIgnoreCase("player2") && linha[53].equalsIgnoreCase("o")) {
                            player2 = "O";
                            player1 = "X";
                        }
                    } else {
                        if (new File("data/savetemp.txt").length() > 0) {
                            tempOut.newLine();
                            tempOut.write(thisLine);
                        } else {
                            tempOut.write(thisLine);
                        }
                        tempOut.flush();
                    }
                }
                tempOut.close();
                in.close();

                Files.deleteIfExists(Paths.get("data/GameSave.txt"));
                new File("data/savetemp.txt").renameTo(new File("data/GameSave.txt"));

                Player loadPlayer1 = new Player(loadPlayer1ID);
                Player loadPlayer2 = new Player(loadPlayer2ID);

                player1_name = loadPlayer1.playerName;
                player2_name = loadPlayer2.playerName;
                player1_id = loadPlayer1.playerId;
                player2_id = loadPlayer2.playerId;
                gameTablePrint();

                if ((roundCount - 1) % 2 == 0) {
                    while (Game.gameRoundCount() < 42) {
                        System.out.println(player1_name + ", em que coluna queres jogar? ");
                        System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                        System.out.print("=> ");
                        gameRoundInput = userInput.next();
                        if (gameRoundInput.equalsIgnoreCase("sair")) {
                            Game.gamePause("player1");
                            Menu.exitMessage();
                        } else {
                            colPlayer1 = Integer.parseInt(gameRoundInput);
                            Game.gameTableRound("player1", colPlayer1);
                        }
                        if (Game.checkIfWin()) {
                            Menu.backOrExit();
                        }
                        System.out.print(player2_name + ", em que coluna queres jogar? ");
                        System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                        System.out.print("=> ");
                        gameRoundInput = userInput.next();
                        if (gameRoundInput.equalsIgnoreCase("sair")) {
                            Game.gamePause("player2");
                            Menu.exitMessage();
                        } else {
                            colPlayer2 = Integer.parseInt(gameRoundInput);
                            Game.gameTableRound("player2", colPlayer2);
                        }
                        if (Game.checkIfWin()) {
                            Menu.backOrExit();
                        }
                    }
                } else {
                    while (Game.gameRoundCount() < 42) {
                        System.out.println(player2_name + ", em que coluna queres jogar? ");
                        System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                        System.out.print("=> ");
                        gameRoundInput = userInput.next();
                        if (gameRoundInput.equalsIgnoreCase("sair")) {
                            Game.gamePause("player2");
                            Menu.exitMessage();
                        } else {
                            colPlayer2 = Integer.parseInt(gameRoundInput);
                            Game.gameTableRound("player2", colPlayer2);
                        }
                        if (Game.checkIfWin()) {
                            Menu.backOrExit();
                        }
                        System.out.print(player1_name + ", em que coluna queres jogar? ");
                        System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                        System.out.print("=> ");
                        gameRoundInput = userInput.next();
                        if (gameRoundInput.equalsIgnoreCase("sair")) {
                            Game.gamePause("player1");
                            Menu.exitMessage();
                        } else {
                            colPlayer1 = Integer.parseInt(gameRoundInput);
                            Game.gameTableRound("player1", colPlayer1);
                        }
                        if (Game.checkIfWin()) {
                            Menu.backOrExit();
                        }
                    }
                }

                if (Game.gameRoundCount() >= 42) {
                    System.out.println("Empate!");
                    Menu.backOrExit();
                }

            }
        } catch (Exception ex) {

        }

    }

    /**
     * Metodo para jogar cada ronda
     * @param player nome do jogador
     * @param col coluna a jogar
     */
    public static void gameTableRound(String player, int col) {

        if (player.equalsIgnoreCase("player1")) {
            play[checkLine(col-1)][col-1] = player1;
        } else if (player.equalsIgnoreCase("player2")) {
            play[checkLine(col-1)][col-1] = player2;
        }

        Menu.clear();

        gameTablePrint();

    }

    /**
     * Mostrar a matriz de jogo
     */
    public static void gameTablePrint() {
        System.out.println("_______________");
        System.out.println("|1|2|3|4|5|6|7|");
        System.out.println("|" + play[5][0] + "|" + play[5][1] + "|" + play[5][2] + "|" + play[5][3] + "|" + play[5][4] + "|" + play[5][5] + "|" + play[5][6] + "|");
        System.out.println("|" + play[4][0] + "|" + play[4][1] + "|" + play[4][2] + "|" + play[4][3] + "|" + play[4][4] + "|" + play[4][5] + "|" + play[4][6] + "|");
        System.out.println("|" + play[3][0] + "|" + play[3][1] + "|" + play[3][2] + "|" + play[3][3] + "|" + play[3][4] + "|" + play[3][5] + "|" + play[3][6] + "|");
        System.out.println("|" + play[2][0] + "|" + play[2][1] + "|" + play[2][2] + "|" + play[2][3] + "|" + play[2][4] + "|" + play[2][5] + "|" + play[2][6] + "|");
        System.out.println("|" + play[1][0] + "|" + play[1][1] + "|" + play[1][2] + "|" + play[1][3] + "|" + play[1][4] + "|" + play[1][5] + "|" + play[1][6] + "|");
        System.out.println("|" + play[0][0] + "|" + play[0][1] + "|" + play[0][2] + "|" + play[0][3] + "|" + play[0][4] + "|" + play[0][5] + "|" + play[0][6] + "|");
        System.out.println("---------------");
    }

    /**
     * Metodo para verificar espaços em branco em cada colina
     * @param line
     * @return
     */
    private static int checkLine(int line) {
        int i = 0;
        while (!(play[i][line].equals(" "))) {
             i += 1;
        }
        return i;
    }


    /**
     * Metodo para verificar quem ganhou o 4emLinha no modo PlayervsPlayer
     */
    public static boolean checkIfWin() {
        //checkWinHorizontal
            int counterX;
            int counterO;
            for (int i = 0; i < play.length; i++) {
                counterX = 0;
                counterO = 0;
                for (int j = 0; j <= play.length; j++) {
                    if (play[i][j].equalsIgnoreCase("x")) {
                        counterX += 1;
                        counterO = 0;
                        if (counterX == 4) {
                            //associar o nome do jogador á letra que ganha!(Resolvido)
                            if (player1 == "X"){
                                System.out.println("O " + player1_name + " ganhou! (horizontal)");
                                saveGameWinData(player1_id);
                                saveNumberVictories(player1_id);
                                return true;
                            }else if (player2 == "X"){
                                System.out.println("O " + player2_name + " ganhou! (horizontal)");
                                saveGameWinData(player2_id);
                                saveNumberVictories(player2_id);
                                return true;
                            }
                        }
                    } else if (play[i][j].equalsIgnoreCase("o")) {
                        counterO += 1;
                        counterX = 0;
                        if (counterO == 4) {
                            //associar o nome do jogador á letra que ganha!(Resolvido)
                            if (player1 == "O"){
                                System.out.println("O " + player1_name + " ganhou! (horizontal)");
                                saveGameWinData(player1_id);
                                saveNumberVictories(player1_id);
                                return true;
                            }else if (player2 == "O"){
                                System.out.println("O " + player2_name + " ganhou! (horizontal)");
                                saveGameWinData(player2_id);
                                saveNumberVictories(player2_id);
                                return true;
                            }
                        }
                    } else {
                        counterO = 0;
                        counterX = 0;
                    }
                }
            }


        //checkWinVertical
            for (int j = 0; j <= play.length; j++) {
                counterX = 0;
                counterO = 0;
                for (int i = 0; i < play.length; i++) {
                    if (play[i][j].equalsIgnoreCase("x")) {
                        counterX += 1;
                        counterO = 0;
                        if (counterX == 4) {
                            //associar o nome do jogador á letra que ganha!(Resolvido)
                            if (player1 == "X"){
                                System.out.println("O " + player1_name + " ganhou! (vertical)");
                                saveGameWinData(player1_id);
                                saveNumberVictories(player1_id);
                                return true;
                            }else if (player2 == "X"){
                                System.out.println("O " + player2_name + " ganhou! (vertical)");
                                saveGameWinData(player2_id);
                                saveNumberVictories(player2_id);
                                return true;
                            }
                        }
                    } else if (play[i][j].equalsIgnoreCase("o")) {
                        counterO += 1;
                        counterX = 0;
                        if (counterO == 4) {
                            //associar o nome do jogador á letra que ganha!(Resolvido)
                            if (player1 == "O"){
                                System.out.println("O " + player1_name + " ganhou! (vertical)");
                                saveGameWinData(player1_id);
                                saveNumberVictories(player1_id);
                                return true;
                            }else if (player2 == "O"){
                                System.out.println("O " + player2_name + " ganhou! (vertical)");
                                saveGameWinData(player2_id);
                                saveNumberVictories(player2_id);
                                return true;
                            }
                        }
                    } else {
                        counterO = 0;
                        counterX = 0;
                    }
                }
            }

        //checkDiagonalBottomLeftWin
            counterX = 0;
            counterO = 0;
            for (int i = 2; i <= 5; i++) {
                if (play[i][i-2].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i-2].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }


            counterX = 0;
            counterO = 0;
            for (int i = 1; i <= 5; i++) {
                if (play[i][i-1].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i-1].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }


            counterX = 0;
            counterO = 0;
            for (int i = 0; i <= 5; i++) {
                if (play[i][i].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }


            counterX = 0;
            counterO = 0;
            for (int i = 0; i <= 5; i++) {
                if (play[i][i+1].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i+1].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }


            counterX = 0;
            counterO = 0;
            for (int i = 0; i <= 4; i++) {
                if (play[i][i+2].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i+2].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }


            counterX = 0;
            counterO = 0;
            for (int i = 0; i <= 3; i++) {
                if (play[i][i+3].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][i+3].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
            }

        //checkDiagonalBottomRightWin
            int j = 3;
            for (int i = 0; i <= 3; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }


            counterX = 0;
            counterO = 0;
            j = 4;
            for (int i = 0; i <= 4; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }


            counterX = 0;
            counterO = 0;
            j = 5;
            for (int i = 0; i <= 5; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }


            counterX = 0;
            counterO = 0;
            j = 6;
            for (int i = 0; i <= 5; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }


            counterX = 0;
            counterO = 0;
            j = 6;
            for (int i = 1; i <= 5; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }


            counterX = 0;
            counterO = 0;
            j = 6;
            for (int i = 2; i <= 5; i++) {
                if (play[i][j].equalsIgnoreCase("x")) {
                    counterX += 1;
                    counterO = 0;
                    if (counterX == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "X"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "X"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else if (play[i][j].equalsIgnoreCase("o")) {
                    counterO += 1;
                    counterX = 0;
                    if (counterO == 4) {
                        //associar o nome do jogador á letra que ganha!(Resolvido)
                        if (player1 == "O"){
                            System.out.println("O " + player1_name + " ganhou! (diagonal)");
                            saveGameWinData(player1_id);
                            saveNumberVictories(player1_id);
                            return true;
                        }else if (player2 == "O"){
                            System.out.println("O " + player2_name + " ganhou! (diagonal)");
                            saveGameWinData(player2_id);
                            saveNumberVictories(player2_id);
                            return true;
                        }
                    }
                } else {
                    counterO = 0;
                    counterX = 0;
                }
                j -= 1;
            }
            return false;
        }


    /**
     * Metodo para guardar no ficheiro quem ganhou o jogo atual
      * @param winner id do vencedor
     */
    public static void saveGameWinData(int winner) {
        try {

            if (!new File("data").exists()) {
                new File("data").mkdir();
            }

            BufferedWriter out = new BufferedWriter(new FileWriter("data/PlayerVsPlayerData.txt", true));
            BufferedReader in = new BufferedReader(new FileReader("data/PlayerVsPlayerData.txt"));

            if (in.ready()) {
                String[] linha;
                int convertedNum = 0;

                while (in.ready()) {
                    linha = in.readLine().split(":");
                    String num = linha[1];
                    convertedNum = Integer.parseInt(num) + 1;
                }
                in.close();

                out.newLine();
                out.write("GameID:" + convertedNum + ":Player1ID:" + player1_id + ":Player2ID:" + player2_id + ":WinnerID:" + winner);
                out.close();
            } else {
                out.write("GameID:1:Player1ID:" + player1_id + ":Player2ID:" + player2_id + ":WinnerID:" + winner);
                out.close();
            }

        } catch (Exception ex) {

        }
    }

    /**
     * Metodo para guardar o número de vitórias do jogador
     * @param winner id do vencedor
     */
    public static void saveNumberVictories(int winner){

        try {

            if (!new File("data").exists()) {
                new File("data").mkdir();
            }


            BufferedWriter temp = new BufferedWriter(new FileWriter("data/temp.txt", true));
            BufferedWriter rankingFile = new BufferedWriter(new FileWriter("data/RankingPlayerVsPlayerData.txt", true));
            BufferedReader inRankingFile = new BufferedReader(new FileReader("data/RankingPlayerVsPlayerData.txt"));
            BufferedReader inRankingFileSecond = new BufferedReader(new FileReader("data/RankingPlayerVsPlayerData.txt"));
            BufferedReader inGameHistory = new BufferedReader(new FileReader("data/PlayerVsPlayerData.txt"));
            rankingFile.close();
            int counter = 0;

            while (inRankingFile.ready()) {
                String linha;
                String[] linhaArray;
                linha = inRankingFile.readLine();
                linhaArray = linha.split(":");
                if (Integer.parseInt(linhaArray[1]) == winner) {
                    counter ++;
                }
            }
            inRankingFile.close();


            if (counter != 0) {
                while (inRankingFileSecond.ready()) {
                    String linha;
                    String[] linhaArray;
                    linha = inRankingFileSecond.readLine();
                    linhaArray = linha.split(":");
                        if (Integer.parseInt(linhaArray[1]) == winner) {
                            temp.write("PlayerID:" + winner + ":Wins:" + (Integer.parseInt(linhaArray[3]) + 1));
                            temp.newLine();
                        } else {
                            temp.write(linha);
                            temp.newLine();
                        }
                }
                temp.close();
                inRankingFileSecond.close();
                } else {
                    int total = 0;
                    while (inGameHistory.ready()) {
                        String linha;
                        String[] linhaArray;
                        linha = inGameHistory.readLine();
                        linhaArray = linha.split(":");
                            if (Integer.parseInt(linhaArray[7]) == winner) {
                                total ++;
                            }
                    }
                    while (inRankingFileSecond.ready()) {
                        String linha;
                        linha = inRankingFileSecond.readLine();
                        temp.write(linha);
                        temp.newLine();
                    }
                    temp.write("PlayerID:" + winner + ":Wins:" + total);
                    temp.close();
                    inGameHistory.close();
                    inRankingFileSecond.close();
                }

            new File("data/RankingPlayerVsPlayerData.txt").delete();
            new File("data/temp.txt").renameTo(new File("data/RankingPlayerVsPlayerData.txt"));

        } catch (Exception ex) {

        }

    }

    /**
     * Metodo para contar o numero de jogadores registados no ranking
     * @return numero de jogadores
     */
    public static int rankingPlayersCount() {
        int linecount = 0;
        try {
            if (!new File("data").exists()) {
                new File("data").mkdir();
            }
            BufferedReader in = new BufferedReader(new FileReader("data/RankingPlayerVsPlayerData.txt"));
            while (in.readLine() != null) {
                linecount ++;
            }
            in.close();
        } catch (Exception ex) {
        }
        return linecount;
    }

    /**
     * Método para mostrar o ranking do modo de jogo PlayervsPlayer
     */
    public static void rankingPrint() {
        try {

            if (!new File("data/RankingPlayerVsPlayerData.txt").exists()) {
                System.out.println("Não existe qualquer jogo pendente...");
                Menu.backOrExit();
            } else {
                int[][] player = new int[rankingPlayersCount()][2];
                BufferedReader in = new BufferedReader(new FileReader("data/RankingPlayerVsPlayerData.txt"));
                String[] linha;
                String thisLine;
                int counter = 0;

                while (in.ready()) {
                    thisLine = in.readLine();
                    linha = thisLine.split(":");
                    player[counter][0] = Integer.parseInt(linha[1]);
                    player[counter][1] = Integer.parseInt(linha[3]);
                    counter ++;
                }
                in.close();
                int tmp1;
                int tmp2;
                int j = 0;
                for(int i = 0; i < player.length - j; i++) {
                    if (player[i][1] > player[i+1][1]) {
                        tmp1 = player[i][0];
                        tmp2 = player[i][1];
                        player[i][0] = player[i+1][0];
                        player[i][1] = player[i+1][1];
                        player[i+1][0] = tmp1;
                        player[i+1][1] = tmp2;
                        j++;
                    }
                }
                int ranking = 1;
                for (int i = player.length - 1; i >= 0; i--) {
                    System.out.println(ranking + ") " + Player.getPlayerName(player[i][0]) + " > " + player[i][1] + " Vitórias");
                    ranking++;
                }
            }

        } catch (Exception ex) {

        }
    }

    public static void tournamentStart() {
        System.out.print("Insira o número de jogadores a participar no torneio: ");
        int tournamentPlayersNumber = userInput.nextInt();
        int[] tournamentPlayers = new int[tournamentPlayersNumber];
        int[][] pointsAndRounds = new int[tournamentPlayersNumber][3];
        int i;
        int j;
        for (i = 0; i < pointsAndRounds.length; i++) {
            pointsAndRounds[i][0] = 0;
            pointsAndRounds[i][1] = 0;
            pointsAndRounds[i][2] = 0;
        }
        Menu.clear();

        for (i = 0; i < tournamentPlayersNumber; i++) {
            System.out.print("Insira o ID do jogador " + (i + 1) + ": ");
            tournamentPlayers[i] = userInput.nextInt();
        }

        for (i = 0; i < tournamentPlayersNumber; i++) {
            for (j = i + 1; j < tournamentPlayersNumber; j++) {
                Menu.clear();
                System.out.println("Próximo Jogo: " + new Player(tournamentPlayers[i]).getName() + " vs " + new Player(tournamentPlayers[j]).getName());
                try {
                    Thread.sleep(2000);
                } catch (Exception ex) {

                }
                Menu.clear();
                gameStartConfig(new Player(tournamentPlayers[i]), new Player(tournamentPlayers[j]));
                gameTableStart();
                while (Game.gameRoundCount() < 42) {
                    System.out.println(new Player(tournamentPlayers[i]).getName() + ", em que coluna queres jogar? ");
                    System.out.print("=> ");
                    gameRoundInput = userInput.next();
                    colPlayer1 = Integer.parseInt(gameRoundInput);
                    Game.gameTableRound("player1", colPlayer1);
                    if (checkIfWin()) {
                        pointsAndRounds[i][0] = tournamentPlayers[i];
                        pointsAndRounds[j][0] = tournamentPlayers[j];
                        pointsAndRounds[i][1] += 3;
                        pointsAndRounds[i][2] += Game.gameRoundCount();
                        pointsAndRounds[j][2] += Game.gameRoundCount();
                        try {
                            Thread.sleep(2000);
                        } catch (Exception ex) {

                        }
                        Menu.clear();
                        break;
                    }
                    System.out.println(new Player(tournamentPlayers[j]).getName() + ", em que coluna queres jogar? ");
                    System.out.print("=> ");
                    gameRoundInput = userInput.next();
                    colPlayer1 = Integer.parseInt(gameRoundInput);
                    Game.gameTableRound("player2", colPlayer1);
                    if (checkIfWin()) {
                        pointsAndRounds[j][0] = tournamentPlayers[j];
                        pointsAndRounds[i][0] = tournamentPlayers[i];
                        pointsAndRounds[j][1] += 3;
                        pointsAndRounds[j][2] += Game.gameRoundCount();
                        try {
                            Thread.sleep(2000);
                        } catch (Exception ex) {

                        }
                        Menu.clear();
                        break;
                    }
                }
                if (Game.gameRoundCount() > 42) {
                    pointsAndRounds[i][0] = tournamentPlayers[i];
                    pointsAndRounds[j][0] = tournamentPlayers[j];
                    pointsAndRounds[i][1] += 1;
                    pointsAndRounds[j][1] += 1;
                    pointsAndRounds[i][2] += Game.gameRoundCount();
                    pointsAndRounds[j][2] += Game.gameRoundCount();
                    System.out.println("Empate!");
                }
            }
        }
        int winnerID = pointsAndRounds[0][0];
        int winnerPoints = pointsAndRounds[0][1];
        int winnerJogadas = pointsAndRounds[0][2];
        for (i = 0; i < pointsAndRounds.length; i++) {
            System.out.println(new Player(pointsAndRounds[i][0]).getName() + "  |  Pontos: " + pointsAndRounds[i][1] + "  |  Total de Jogadas: " + pointsAndRounds[i][2]);
            if (pointsAndRounds[i][1] == winnerPoints && pointsAndRounds[i][2] < winnerJogadas) {
                winnerID = pointsAndRounds[i][0];
                winnerPoints = pointsAndRounds[i][1];
                winnerJogadas = pointsAndRounds[i][2];
                break;
            } else if (pointsAndRounds[i][1] > winnerPoints) {
                winnerID = pointsAndRounds[i][0];
                winnerPoints = pointsAndRounds[i][1];
                winnerJogadas = pointsAndRounds[i][2];
            }
        }
        System.out.println();
        System.out.println(new Player(winnerID).getName() + " venceu com " + winnerJogadas + " jogadas!");
        Menu.backOrExit();
    }

}

