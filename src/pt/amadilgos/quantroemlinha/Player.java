package pt.amadilgos.quantroemlinha;
/**
 * @author Fernando Sousa e Ruben Marinheiro
 * @version 1.0
 * @since 2018-12-10
 */
import java.io.*;

/**
 * Representa os dados do jogador
 */
public class Player extends Person{

    public int playerId;
    public int playerWinCount;
    public int playerTournamentCount;

    /**
     * Cria um jogador
     * @param name nome do jogador
     * @param age idade do jogador
     */
    public Player(String name, int age) {
        this.playerId = getPlayerId();
        this.playerName = name;
        this.playerAge = age;
        this.playerWinCount = 0;
        this.playerTournamentCount = 0;
    }

    /**
     * Cria um objeto "jogador" a partir de um id
     * @param id id do jpgador
     */
    public Player(int id) {

        try {
            BufferedReader in = new BufferedReader(new FileReader("data/PlayersData.txt"));

            String[] linha;
            String thisLine;

            while (in.ready()) {
                thisLine = in.readLine();
                if (thisLine.startsWith(id + ":")) {
                    linha = thisLine.split(":");
                    this.playerId = id;
                    this.playerName = linha[1];
                    this.playerAge = Integer.parseInt(linha[2]);
                    this.playerWinCount = Integer.parseInt(linha[3]);
                    this.playerTournamentCount = Integer.parseInt(linha[4]);
                }
            }
            in.close();

        } catch (Exception ex) {

        }
    }

    /**
     * Getter - retornar id do jogador
     * @return retorna o id do jogador
     */
    public int getPlayerId() {
        setPlayerId();
        return this.playerId;
    }

    /**
     * Setter - criar o id do jogador
     */
    public void setPlayerId() {
        try {

            if (!new File("data").exists()) {
                this.playerId = 1;
            } else {
                if (!new File("data/PlayersData.txt").exists()) {
                    this.playerId = 1;
                } else {
                    BufferedReader in = new BufferedReader(new FileReader("data/PlayersData.txt"));
                    if (in.ready()) {
                        String[] linha;
                        int convertedNum = 0;
                        while (in.ready()) {
                            linha = in.readLine().split(":");
                            String num = linha[0];
                            convertedNum = Integer.parseInt(num) + 1;
                        }
                        this.playerId = convertedNum;
                        in.close();
                    } else {
                        this.playerId = 1;
                    }
                }
            }

        } catch (Exception ex) {

        }
    }

    /** Gravar no ficheiro os dados do jogador
     *
     * @param selectedPlayer jogador a ser criado no momento
     */
    public static void savePlayerData(Player selectedPlayer) {
        try {

            if (!new File("data").exists()) {
                new File("data").mkdir();
            }
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("data/PlayersData.txt", true), "UTF8"));
            BufferedReader in = new BufferedReader(new FileReader("data/PlayersData.txt"));

            if (in.ready()) {
                out.newLine();
                out.write(selectedPlayer.playerId + ":" + selectedPlayer.playerName + ":" + selectedPlayer.playerAge + ":" + selectedPlayer.playerWinCount + ":" + selectedPlayer.playerTournamentCount);
                out.close();
            } else {
                out.write(selectedPlayer.playerId + ":" + selectedPlayer.playerName + ":" + selectedPlayer.playerAge + ":" + selectedPlayer.playerWinCount + ":" + selectedPlayer.playerTournamentCount);
                out.close();
            }
            in.close();

        } catch (Exception ex) {

        }
    }

    /** Consultar nome do jogador através do seu ID
     *
     * @param id id do jogador a ser consultado
     */
    public static String getPlayerName(int id) {
        String outputName = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader("data/PlayersData.txt"));
            String[] linha;
            String thisLine;

            while (in.ready()) {
                thisLine = in.readLine();
                linha = thisLine.split(":");
                if (linha[0].equalsIgnoreCase(String.valueOf(id))) {
                    outputName = linha[1];
                }
            }
            in.close();
        } catch (Exception ex) {

        }
        return outputName;
    }

}
