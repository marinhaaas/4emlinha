package pt.amadilgos.quantroemlinha;
/**
 * @author Fernando Sousa e Ruben Marinheiro
 * @version 1.0
 * @since 2018-12-10
 */


/**
 * Cria o nome e idade do jogador
 */
public class Person {

    public String playerName;
    public int playerAge;

    /**
     * retornar o nome do jogador
     * @return playerName que é o nome do jogador
     */
    public String getName() {
        return playerName;
    }

    /**
     * dar nome ao jogador
     * @param name nome do jogador
     */
    public void setName(String name) {
        this.playerName = name;
    }

    /**
     * retorna a idade do jogador
     * @return playerAge idade do jogador
     */
    public int getAge() {
        return playerAge;
    }

    /**
     * atribuir idade ao jogador
     * @param age idade do jogador
     */
    public void setAge(int age) {
        this.playerAge = age;
    }
}
