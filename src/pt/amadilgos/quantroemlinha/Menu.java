package pt.amadilgos.quantroemlinha;
/**
 * @author Fernando Sousa e Ruben Marinheiro
 * @version 1.0
 * @since 2018-12-10
 */
import java.util.Scanner;

public class Menu {
    static Scanner userInput = new Scanner(System.in);
    static int colPlayer1;
    static int colPlayer2;
    static String gameRoundInput;


    /**
     * Metodo para criar um ecrã temporario para apresentação
     */
    public static void splashScreen() {
        System.out.println("+----------------+");
        System.out.println("|   4 EM LINHA   |");
        System.out.println("|      V1.0      |");
        System.out.println("+----------------+");
        System.out.println("By Fernando Sousa & Rúben Marinheiro");
        System.out.println();

        try {
            Thread.sleep(2000);
        } catch (Exception ex) {

        }
    }


    /**
     * Metodo para criar menu e em seguida escolher a opção que os jogadores preferem
     */
    public static void startMenu() {

        int option = 0;
        String selectedOption = "";
        int times = 0;

        do {

            clear();

            if (times != 0) {
                System.out.println("Inseriu uma opção inválida, tente mais uma vez...");
                System.out.println();
            }

            System.out.println("Deseja:");
            System.out.println("      1 => Criar Novo Jogador");
            System.out.println("      2 => Jogar Contra Jogador");
            System.out.println("      3 => Continuar Jogo");
            System.out.println("      4 => Criar Torneio");
            System.out.println("      5 => Consultar Ranking");
            System.out.println("      6 => Sair");
            System.out.println();
            System.out.print("Insira uma das opções: ");

            selectedOption = userInput.next();
            clear();
            times++;

        } while (!selectedOption.equals("1") && !selectedOption.equals("2") && !selectedOption.equals("3") && !selectedOption.equals("4") && !selectedOption.equals("5") && !selectedOption.equals("6") && !selectedOption.equals("7"));

        option = Integer.parseInt(selectedOption);
        switch (option) {

            case 1:
                System.out.print("Insira o seu nome: ");
                userInput.nextLine();
                String playerName = userInput.nextLine();
                System.out.print("Insira a sua idade: ");
                int playerAge = userInput.nextInt();
                Player newPlayer = new Player(playerName, playerAge);
                Player.savePlayerData(newPlayer);
                System.out.println("O seu ID é: " + (newPlayer.getPlayerId() - 1) + ", MEMORIZE!");
                try {
                    Thread.sleep(3500);
                } catch (Exception ex) {

                }
                for(int clear = 0; clear < 10; clear++) {
                    System.out.println("\b") ;
                }
                backOrExit();
                break;
            case 2:
                System.out.print("Insira o ID do primeiro jogador: ");
                int firstID = userInput.nextInt();
                System.out.print("Insira o ID do segundo jogador: ");
                int secondID = userInput.nextInt();

                Player selectedPlayer1 = new Player(firstID);
                Player selectedPlayer2 = new Player(secondID);

                Game.gameStartConfig(selectedPlayer1, selectedPlayer2);
                Game.gameTableStart();

                while (Game.gameRoundCount() < 42) {
                    System.out.println(selectedPlayer1.getName() + ", em que coluna queres jogar? ");
                    System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                    System.out.print("=> ");
                    gameRoundInput = userInput.next();
                    if (gameRoundInput.equalsIgnoreCase("sair")) {
                        Game.gamePause("player1");
                    } else {
                        colPlayer1 = Integer.parseInt(gameRoundInput);
                        Game.gameTableRound("player1", colPlayer1);
                    }
                    if (Game.checkIfWin()) {
                        backOrExit();
                    }
                    System.out.print(selectedPlayer2.getName() + ", em que coluna queres jogar? ");
                    System.out.println("Preferes parar o jogo tal como está? Insere \"sair\".");
                    System.out.print("=> ");
                    gameRoundInput = userInput.next();
                    if (gameRoundInput.equalsIgnoreCase("sair")) {
                        Game.gamePause("player2");
                    } else {
                        colPlayer2 = Integer.parseInt(gameRoundInput);
                        Game.gameTableRound("player2", colPlayer2);
                    }
                    if (Game.checkIfWin()) {
                        backOrExit();
                    }
                }
                if (Game.gameRoundCount() >= 42) {
                    System.out.println("Empate!");
                    Menu.backOrExit();
                }
                break;
            case 3:
                System.out.print("Insira o ID do jogo: ");
                int gameLoadID = userInput.nextInt();

                Game.gameSaveLoad(gameLoadID);
                Game.gameLoadConfig(gameLoadID);
                break;
            case 4:
                Game.tournamentStart();
                break;
            case 5:
                Game.rankingPrint();
                break;
            case 6:
                exitMessage();
        }
    }


    /**
     * Método para usar quando se quer voltar ao menu ou sair do programa
     * depois de se criar um jogo ou em situação de empate no modo de
     * jogo PlayervsPlayer
     */
    public static void backOrExit() {
        String selectedOption = "";
        int times = 0;
        do {
            if (times != 0) {
                System.out.println("Inseriu uma opção inválida, tente mais uma vez...");
                System.out.println();
            }

            System.out.println("Deseja:");
            System.out.println("      1 => Voltar ao Menu");
            System.out.println("      2 => Sair");
            selectedOption = userInput.next();
            times++;
        } while (!selectedOption.equals("1") && !selectedOption.equals("2"));

        switch (Integer.parseInt(selectedOption)) {
            case 1:
                startMenu();
                break;
            case 2:
                exitMessage();
                break;
        }
    }

    /**
     * Método criado e usado para "limpar" ecrã
     */
    public static void clear() {
        for(int clear = 0; clear < 100; clear++) {
            System.out.println("\b") ;
        }
    }

    /**
     * Método criado e usado para escrever uma mensagem de despedida cada vez que o programa termina
     */
    public static void exitMessage() {
        clear();
        System.out.println("Obrigado pela oportunidade que deu à nossa aplicação, até à próxima!");
        System.exit(1);
    }

}
