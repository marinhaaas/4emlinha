package pt.amadilgos.quantroemlinha;
/**
 * @author Fernando Sousa e Ruben Marinheiro
 * @version 1.0
 * @since 2018-12-10
 */

/**
 * Main usada para iniciar o jogo e ir para o menu
 */
public class Main{
    public static void main(String[] args) {
        Menu.splashScreen();
        Menu.startMenu();
    }

}

//http://prntscr.com/ltu87f
